
// Exercise 2
// Faisel Al

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	DIAMOND, HEART, CLUB, SPADE
};

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case TWO: cout << "two "; break;
	case THREE: cout << "three "; break;
	case FOUR: cout << "four "; break;
	case FIVE: cout << "five "; break;
	case SIX: cout << "six "; break;
	case SEVEN: cout << "seven "; break;
	case EIGHT: cout << "eight "; break;
	case NINE: cout << "nine "; break;
	case TEN: cout << "ten "; break;
	case JACK: cout << "jack "; break;
	case QUEEN: cout << "queen "; break;
	case KING: cout << "king "; break;
	case ACE: cout << "ace "; break;
	}

	cout << "of ";
	switch (card.suit)
	{
	case DIAMOND: cout << "diamonds\n"; break;
	case HEART: cout << "hearts\n"; break;
	case CLUB: cout << "clubs\n"; break;
	case SPADE: cout << "spades\n"; break;
	}

}

//todo: discuss with team
Card HighCard(Card card1, Card card2)
{
	if (card1.rank >= card2.rank) return card1;
	return card2;
}

int main()
{
	Card c;
	Card c2;

	c.rank = SEVEN;
	c.suit = CLUB;

	c2.rank = SEVEN;
	c2.suit = HEART;

	PrintCard(HighCard(c, c2));

	_getch();
	return 0;
}
